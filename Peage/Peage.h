//
// Created by antony on 23/05/17.
//

#ifndef PEAGES_PEAGE_H
#define PEAGES_PEAGE_H

#include <vector>

using namespace std;

class Voiture;
class Peage {
public:
    unsigned long fileDAttente();
    int rentrerFileDAttente(Voiture* voiture);

    Peage();

private:
    vector<Voiture*> *voitures;
};


#endif //PEAGES_PEAGE_H
