#include <iostream>
#include "SDL2/SDL.h"
#include "Voiture/Voiture.h"

/* Very simple thread - counts 0 to 9 delaying 50ms between increments */
static int TestThread(void *ptr)
{
    int cnt;
    vector<Peage*> *peages = new vector<Peage*>();
    peages->push_back(new Peage());
    peages->push_back(new Peage());
    peages->push_back(new Peage());
    peages->push_back(new Peage());
    for (cnt = 0; cnt < 10; ++cnt) {
        printf("\nThread counter: %d", cnt);
        Voiture* voiture = new Voiture(*peages);
    }

    return cnt;
}

int main(int argc, char *argv[])
{
    SDL_Thread *thread;
    int         threadReturnValue;

    printf("\nSimple SDL_CreateThread test:");

    /* Simply create a thread */
    thread = SDL_CreateThread(TestThread, "TestThread", (void *)NULL);

    if (NULL == thread) {
        printf("\nSDL_CreateThread failed: %s\n", SDL_GetError());
    } else {
        SDL_WaitThread(thread, &threadReturnValue);
        printf("\nThread returned value: %d", threadReturnValue);
    }

    return 0;
}