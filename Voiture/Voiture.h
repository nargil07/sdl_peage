//
// Created by antony on 23/05/17.
//

#ifndef PEAGES_VOITURE_H
#define PEAGES_VOITURE_H

#include <vector>
#include "../Peage/Peage.h"

using namespace std;
class Voiture {
public:
    bool prendreTicket();
    Voiture(vector<Peage*> const &peages);

private:
    Peage* peage;
};


#endif //PEAGES_VOITURE_H
